{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Intro to ONNX Runtime\n",
    "\n",
    "[ONNX Runtime](https://onnxruntime.ai/) provides an easy way to run machine learned models with high performance on [CPU](https://pypi.org/project/onnxruntime/) or [GPU](https://pypi.org/project/onnxruntime-gpu/) without dependencies on the training framework. Machine learning frameworks are usually optimized for batch training rather than for prediction, which is a more common scenario in applications, sites, and services. At a high level, you can:\n",
    "\n",
    "* Train a model using your favorite framework.\n",
    "* Convert or export the model into ONNX format. See ONNX Tutorials for more details.\n",
    "* Load and run the model using ONNX Runtime."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Step 1: Train a model using your favorite framework"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sklearn.datasets import load_iris\n",
    "from sklearn.model_selection import train_test_split\n",
    "iris = load_iris()\n",
    "x, y = iris.data, iris.target\n",
    "x_train, x_test, y_train, y_test = train_test_split(x, y)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sklearn.linear_model import LogisticRegression\n",
    "clr = LogisticRegression()\n",
    "clr.fit(x_train, y_train)\n",
    "print(clr)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Step 2: Convert or export the model into ONNX format\n",
    "\n",
    "[Open Neural Network Exchange (ONNX](https://github.com/onnx/onnx) is an open ecosystem that empowers AI developers to choose the right tools as their project evolves. ONNX provides an open source format for AI models, both deep learning and traditional ML. It defines an extensible computation graph model, as well as definitions of built-in operators and standard data types. Currently we focus on the capabilities needed for inferencing (scoring).\n",
    "\n",
    "ONNX is [widely supported](https://onnx.ai/supported-tools) and can be found in many frameworks, tools, and hardware. Enabling interoperability between different frameworks and streamlining the path from research to production helps increase the speed of innovation in the AI community. We invite the community to join us and further evolve ONNX.\n",
    "\n",
    "ONNX has a wide range of converters that can be found under [ONNXMLTools](https://github.com/onnx/onnxmltools)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from skl2onnx import convert_sklearn\n",
    "from skl2onnx.common.data_types import FloatTensorType\n",
    "\n",
    "initial_type = [('float_input', FloatTensorType([None, 4]))]\n",
    "onx = convert_sklearn(clr, initial_types=initial_type)\n",
    "with open(\"logreg_iris.onnx\", \"wb\") as f:\n",
    "    f.write(onx.SerializeToString())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's try to plot the model pipeline."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from onnx import ModelProto\n",
    "model = ModelProto()\n",
    "with open(output_path, 'rb') as fid:\n",
    "    content = fid.read()\n",
    "    model.ParseFromString(content)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!sudo apt install graphviz\n",
    "\n",
    "from onnx.tools.net_drawer import GetPydotGraph, GetOpNodeProducer\n",
    "pydot_graph = GetPydotGraph(model.graph, name=model.graph.name, rankdir=\"LR\",\n",
    "                            node_producer=GetOpNodeProducer(\"docstring\"))\n",
    "pydot_graph.write_dot(\"graph.dot\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import os\n",
    "os.system('dot -O -Tpng graph.dot')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Step 3: Load and run the model using ONNX Runtime"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy\n",
    "import onnxruntime as ort\n",
    "\n",
    "sess = ort.InferenceSession(\n",
    "    \"logreg_iris.onnx\", providers=ort.get_available_providers())\n",
    "input_name = sess.get_inputs()[0].name\n",
    "label_name = sess.get_outputs()[0].name\n",
    "pred_onx = sess.run(\n",
    "    [label_name], {input_name: x_test.astype(numpy.float32)})[0]\n",
    "print(pred_onx)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Important Links"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* [ONNX Versioning](https://github.com/onnx/onnx/blob/main/docs/Versioning.md)\n",
    "* [ONNX Opsets and Operators](https://github.com/onnx/onnx/blob/main/docs/Operators.md)\n",
    "* [ONNX Model Zoo](https://github.com/onnx/models)\n",
    "* [The ONNX Runtime (ORT) Python API](https://onnxruntime.ai/docs/api/python/api_summary.html)\n",
    "* TensorRT/ONNX integration [1](https://docs.nvidia.com/deeplearning/tensorrt/quick-start-guide/index.html), [2](https://github.com/onnx/onnx-tensorrt/blob/main/README.md)\n",
    "* [ONNX Examples](https://github.com/onnx/tutorials)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# TensorFlow + ONNX\n",
    "We use [tf2onnx](https://github.com/onnx/tensorflow-onnx) to convert TF model to ONNX. PyTorch has a native converter.\n",
    "\n",
    "Some TensorFlow ops will fail to convert if the ONNX opset used is too low. Use the largest opset compatible with your application."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import tensorflow as tf\n",
    "import tf2onnx\n",
    "import onnx\n",
    "\n",
    "model = tf.keras.Sequential()\n",
    "model.add(tf.keras.layers.Dense(4, activation=\"relu\"))\n",
    "\n",
    "input_signature = [tf.TensorSpec([3, 3], tf.float32, name='x')]\n",
    "# Use from_function for tf functions\n",
    "onnx_model, _ = tf2onnx.convert.from_keras(model, input_signature, opset=13)\n",
    "onnx.save(onnx_model, \"model.onnx\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Example image.\n",
    "!wget -q https://github.com/onnx/tensorflow-onnx/raw/main/tests/ade20k.jpg"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import os\n",
    "import tensorflow as tf\n",
    "from tensorflow.keras.applications.resnet50 import ResNet50\n",
    "from tensorflow.keras.preprocessing import image\n",
    "from tensorflow.keras.applications.resnet50 import preprocess_input, decode_predictions\n",
    "import numpy as np\n",
    "\n",
    "img_path = 'ade20k.jpg'\n",
    "\n",
    "img = image.load_img(img_path, target_size=(224, 224))\n",
    "\n",
    "x = image.img_to_array(img)\n",
    "x = np.expand_dims(x, axis=0)\n",
    "x = preprocess_input(x)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "model = ResNet50(weights='imagenet')\n",
    "\n",
    "preds = model.predict(x)\n",
    "print('Keras Predicted:', decode_predictions(preds, top=3)[0])\n",
    "model.save(os.path.join(\"/tmp\", model.name))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's convert our model to ONNX."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import tf2onnx\n",
    "import onnxruntime as ort\n",
    "\n",
    "spec = (tf.TensorSpec((None, 224, 224, 3), tf.float32, name=\"input\"),)\n",
    "output_path = model.name + \".onnx\"\n",
    "\n",
    "model_proto, _ = tf2onnx.convert.from_keras(model, input_signature=spec, opset=13, output_path=output_path)\n",
    "output_names = [n.name for n in model_proto.graph.output]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "providers = ['CPUExecutionProvider']\n",
    "sess = ort.InferenceSession(output_path, providers=providers)\n",
    "onnx_pred = sess.run(output_names, {\"input\": x})\n",
    "\n",
    "print('ONNX Predicted:', decode_predictions(onnx_pred[0], top=3)[0])\n",
    "\n",
    "# make sure ONNX and keras have the same results\n",
    "np.testing.assert_allclose(preds, onnx_pred[0], rtol=1e-5)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "ONNX models have metadata that help us understand compatibility."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "meta = sess.get_modelmeta()\n",
    "\n",
    "print(\"custom_metadata_map={}\".format(meta.custom_metadata_map))\n",
    "print(\"description={}\".format(meta.description))\n",
    "print(\"domain={}\".format(meta.domain, meta.domain))\n",
    "print(\"graph_name={}\".format(meta.graph_name))\n",
    "print(\"producer_name={}\".format(meta.producer_name))\n",
    "print(\"version={}\".format(meta.version))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can use Netron to debug and visualize our model."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import netron\n",
    "netron.start(output_path, 8081)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can also profile the execution of our model."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "options = ort.SessionOptions()\n",
    "options.enable_profiling = True\n",
    "sess_profile = ort.InferenceSession(output_path, options, providers=providers)\n",
    "input_name = sess.get_inputs()[0].name\n",
    "\n",
    "sess.run(None, {input_name: x})\n",
    "prof_file = sess_profile.end_profiling()\n",
    "print(prof_file)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "interpreter": {
   "hash": "27256f289ae4540779ca300de5375291a8377d4d44813a8ff74370c43ffffe89"
  },
  "kernelspec": {
   "display_name": "Python 3.9.11 ('git-tutorial')",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.12"
  },
  "orig_nbformat": 4
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
